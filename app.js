'use strict';

// [START gae_node_request_example]

const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');

app.get('/robots.txt', function (req, res) {
  res.type('text/plain');
  res.send("User-agent: *\nAllow: /");
});

app.use(cors());

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
 extended: true
}));

app.use(express.static('public'))

app.get('/', (req, res) => {
  res.send('Behired Alive');
});

app.listen(8080, () => {
  console.log('Behired - Listening on port 8080!');
});

app.get('/google7f7e8cda3e8ddd48.html', (req, res) => {
  res.sendFile(path.join(__dirname + '/google7f7e8cda3e8ddd48.html'));
});

require(path.join(__dirname, 'app/routes/email.route'))(app);
require(path.join(__dirname, 'app/routes/google.route'))(app);
require(path.join(__dirname, 'app/routes/cvlibrary.route'))(app);
require(path.join(__dirname, 'app/routes/reed.route'))(app);
require(path.join(__dirname, 'app/routes/adzuna.route'))(app);
require(path.join(__dirname, 'app/routes/payment.route'))(app);
require(path.join(__dirname, 'app/routes/behired.route'))(app);
require(path.join(__dirname, 'app/routes/xml-generator.route'))(app);
require(path.join(__dirname, 'app/routes/json-generator.route'))(app);
require(path.join(__dirname, 'app/routes/mailing.route'))(app);
require(path.join(__dirname, 'app/routes/expirer.route'))(app);
// require(path.join(__dirname, 'app/routes/subscription.route'))(app);
require(path.join(__dirname, 'app/routes/test.route'))(app);

// [END gae_node_request_example]

module.exports = app;