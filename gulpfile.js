'use strict';

var gulp = require('gulp'),
  nodemon = require('gulp-nodemon');

gulp.task('start', function () {
    nodemon({
      script: 'app.js',
      ext: 'js html'
    })
    .on('restart', function () {
      console.log('Server Restarted!');
    })
});

gulp.task('serve', ['start']);
