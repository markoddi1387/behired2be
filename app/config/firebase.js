const admin = require("firebase-admin");
const serviceAccount = require("../../leisure-project-firebase-adminsdk-orvkt-4f7de6100d.json");
const settings = { timestampsInSnapshots: true };

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://leisure-project.firebaseio.com"
});

admin.firestore().settings(settings);

module.exports.firebaseLeisure = admin.database();
module.exports.firebaseLeisureStore = admin.firestore();
