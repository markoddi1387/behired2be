const express = require('express');
const router = express.Router();

const paymentController = require('../controllers/payment.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/pay/534a5c7146a447349341ac99435a9bdb').post(paymentController.createPayment);
};
