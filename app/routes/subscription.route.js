const express = require('express');
const router = express.Router();

const paymentController = require('../controllers/payment.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/subscribe/534a5c7146a4473493gfdeu9435a9bdb').post(paymentController.subscribe);
  router.route('/unsubscribe/534a5c7146a4473493gfdeu9435a9bdb').post(paymentController.unsubscribe);
};
