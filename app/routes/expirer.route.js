const express = require('express');
const router = express.Router();

const expirerController = require('../controllers/expirer.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/expirer/534a5c71464447669341a779435a1o1odun').get(expirerController.expireJobs);
};
