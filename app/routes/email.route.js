const express = require('express');
const router = express.Router();

const emailController = require('../controllers/email.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/email').post(emailController.sendEmail);
};
