const express = require('express');
const router = express.Router();

const behiredController = require('../controllers/behired.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/behired/jobs-board/json').get(behiredController.getBehiredJson);
  router.route('/behired/jobs-board/xml').get(behiredController.getBehiredXml);
  router.route('/adzuna/jobs').get(behiredController.getAdzunaXml);
  router.route('/sitemap').get(behiredController.getBehiredSitemap);
  router.route('/behired/jobs-board/indeed/xml').get(behiredController.getIndeedXml);
  router.route('/jobs-board').get(behiredController.getBehiredHtml);
};
