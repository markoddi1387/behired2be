const express = require('express');
const router = express.Router();

const mailingController = require('../controllers/mailing-list.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/mailing/534a5c71464447669341a779435aiazbx').get(mailingController.startMailingList);
  router.route('/mailing-blog/534a5caa464427899341a77rfd6tiazbx').post(mailingController.subscribeToBlogMailingList);
};
