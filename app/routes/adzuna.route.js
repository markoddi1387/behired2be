const express = require('express');
const router = express.Router();

const adzunaController = require('../controllers/adzuna.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/get/adzuna/jobs/534a5c7146a447349341ac99435a9bdb').get(adzunaController.startAdzunaRequests);
};
