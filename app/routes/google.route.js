const express = require('express');
const router = express.Router();

const googleController = require('../controllers/google.controller');

module.exports = (app) => {
  app.use('/', router);
  // router.route('/google/post/534a5c7146a447349341ac99435a9bdb').post(googleController.postGoogleJob);
};
