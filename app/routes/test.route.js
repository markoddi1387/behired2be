const express = require('express');
const router = express.Router();

const testController = require('../controllers/test.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/test/534a5c7146a447349341ac99435a9bdb').get(testController.addtestJob);
};
