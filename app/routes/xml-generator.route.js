const express = require('express');
const router = express.Router();

const xmlController = require('../controllers/xml-generator.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/write/xml/534a5c7146a447349341ac99435a9bdb').get(xmlController.generateXmlFiles);
};
