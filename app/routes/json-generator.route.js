const express = require('express');
const router = express.Router();

const jsonController = require('../controllers/json-generator.controller');

module.exports = (app) => {
  app.use('/', router);
  router.route('/write/json/190a5c7146a448239341ac99435a9bdb').get(jsonController.generateJsonFiles);
};
