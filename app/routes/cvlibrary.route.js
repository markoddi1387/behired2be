const express = require('express');
const router = express.Router();

const cvlibraryController = require('../controllers/cvlibrary.controller');

module.exports = (app) => {
  app.use('/', router);
  // router.route('/get/cvlibrary/jobs/534a5c7146a447349341ac99435a9bdb').get(cvlibraryController.getCVLibraryJobs);
};
