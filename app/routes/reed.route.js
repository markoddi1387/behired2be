const express = require('express');
const router = express.Router();

const reedController = require('../controllers/reed.controller');

module.exports = (app) => {
  app.use('/', router);
  // router.route('/get/reed/jobs/534a5c7146a447349341ac99435a9bdb').get(reedController.getReedJobs);
};
