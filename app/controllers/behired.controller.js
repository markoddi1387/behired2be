const fs = require('fs');

exports.getBehiredXml = (req, res) => {
  fs.readFile(`${__dirname}/xml/behired.xml`, 'utf8', (err, data) => {
    if (!err) {
      res.send(data);
    } else {
      res.send(404);
    }
  });
};

exports.getAdzunaXml = (req, res) => {
  fs.readFile(`${__dirname}/xml/adzuna.xml`, 'utf8', (err, data) => {
    if (!err) {
      res.send(data);
    } else {
      res.send(404);
    }
  });
};

exports.getBehiredSitemap = (req, res) => {
  fs.readFile(`${__dirname}/xml/sitemap.xml`, 'utf8', (err, data) => {
    if (!err) {
      res.send(data);
    } else {
      res.send(404);
    }
  });
};

exports.getIndeedXml = (req, res) => {
  fs.readFile(`${__dirname}/xml/indeed.xml`, 'utf8', (err, data) => {
    if (!err) {
      res.send(data);
    } else {
      res.send(404);
    }
  });
};

exports.getBehiredJson = (req, res) => {
  const rawDate = fs.readFileSync(`${__dirname}/json/behired.json`);
  const data = JSON.parse(rawDate);  
  res.send(data);
};

exports.getBehiredHtml = (req, res) => {
  res.sendFile(`${__dirname}/jobs.html`);
};
