const database = require("../config/firebase");
const fs = require("fs");

exports.generateJsonFiles = (req, res) => {
  const snapshot = database.firebaseLeisureStore.collection("behired");
  let listOfJobs = [];

  snapshot.get().then((jobs) => {
    jobs.forEach((job) => {
      let newJob = job.data();
      newJob["id"] = job.id;

      listOfJobs = [ ...listOfJobs, newJob ];
    });

    createJson(listOfJobs);
    createHtml(listOfJobs);
    res.sendStatus(200);
  });
};

createJson = (jobs) => {
  let listOfJobs = [];

  jobs.forEach(job => {
    id = job.id;
    job = job["d"];
    
    let jsonJob = {
      "@context": "http://schema.org",
      "@type": "JobPosting",
      "title" : job.name,
      "description" : job.description,  
      "datePosted" : "2019-02-24",
      "validThrough": "2019-03-24",
      "hiringOrganization": {
        "@type": "Organization",
        "name": "Behired",
        "sameAs": "https://behired.co.uk"
      },
      "jobLocation": {
        "@type": "Place",
        "address": {
          "@type": "PostalAddress",
          "streetAddress": job.address.addressLine1,
          "postalCode": job.address.postCode,
          "addressCountry": "UK"
        }
      },
      "baseSalary": {
        "@type": "MonetaryAmount",
        "currency": "GBP",
        "value": {
          "@type": "QuantitativeValue",
          "value": job.pay
        }
      },
      "industry": "Leisure",
      "url": `https://behired.co.uk/job/${ id }`
    }

    listOfJobs = [ ...listOfJobs, jsonJob ];
  });

  let json = JSON.stringify(listOfJobs);  
  fs.writeFileSync(`${__dirname}/json/behired.json`, json);  

  return true;
};

createHtml = (jobs) => {
  let listOfJobs = `
    <!doctype html><html lang="en">
    <head>
      <title>Behired | Jobs</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate, post-check=0, pre-check=0">
      <meta http-equiv="Pragma" content="no-cache">
      <meta http-equiv="Expires" content="0">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="author" content="Behired">
      <meta name="application-name" content="Behired">
      <meta name="description" content="The most efficient way to Behired, sign up today for free and begin your journey with us">
      <meta name="keywords" content="Behired, Personal Trainers, Behired Jobs, Swim Jobs, Swimming Teacher, recruitment, hiring, hire, yogo, jobs, job board, catering, trainers, admin, coaches, Staffing">
      <meta name="application-name" content="Behired">
    </head><body>
  `;

  jobs.forEach(job => {
    let id = job.id;
    job = job["d"];
    job["url"] = `https://behired.co.uk/job/${ id }`;

    listOfJobs += `<div itemscope itemtype="http://schema.org/JobPosting">
        <img src="logo.png" alt="Behired.co.uk" itemprop="image">
        <h2 itemprop="title">${ job.name }</h2>
        <p><a itemprop="url" href="${ job.url }">Apply at Behired</a></p>
        <span>
          <p><strong>Location:</strong> 
            <span itemprop="jobLocation" itemscope itemtype="http://schema.org/Place">
              <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="addressRegion">UK</span>
                <span itemprop="streetAddress">${ job.address.addressLine1 }</span>
                <span itemprop="postalCode">KT64DF</span>
                <span itemprop="addressLocality">UK</span>
                <span itemprop="addressCountry">England</span>
              </span>
            </span>
          </p>
        </span>
        <span>
          <p><strong>Base Salary:</strong> 
            <span itemprop="baseSalary" itemscope itemtype="http://schema.org/MonetaryAmount">
              <span itemprop="currency">GBP</span>
              <span itemprop="value">${ job.pay }</span>
            </span>
          </p>
        </span> 
        <p><strong>Industry:</strong> <span itemprop="industry">Leisure</span>
        <br><strong>Occupational Category:</strong> <span itemprop="occupationalCategory">Leisure</span>
        <br><strong>Hours:</strong> <span itemprop="employmentType">Full-time</span>, <span itemprop="workHours">40 hours per week</span>
        <br><strong>Salary:</strong> 
        <span itemprop="salaryCurrency">GBP</span> 
        </p>
        <p itemprop="description">
          <strong>Description:</strong> 
          <span itemprop="hiringOrganization" itemscope itemtype="http://schema.org/Organization">
            <span itemprop="name">Behired.co.uk</span>
            ${ job.description }    
          </span>
        </p>
        <p>Date Posted: <span itemprop="datePosted">2019-03-05</span></p>
        <p>Valid Through: <span itemprop="validThrough">2019-05-24</span></p>
      </div>`;
  });

  listOfJobs += `</body></html>`;

  fs.writeFileSync(`${__dirname}/jobs.html`, listOfJobs);  

  return true;
};




