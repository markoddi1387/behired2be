const sendgrid = require('sendgrid')('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');
const emailTemplateId = 'dabbcecf-e490-420b-85e8-d3e8acb509c1';

exports.sendEmail = (req, res) => {
  const token = req.headers.authorization;

  if (token !== 'mango') {
    res.send(JSON.stringify({ 'response' : 'Error: Authorization' }));
  } else {
    const { email, subject, body } = req.body;
    let emailTemplate = new sendgrid.Email();
    emailTemplate.addTo(email);
    emailTemplate.subject = subject;
    emailTemplate.from = 'noreply@behired.co.uk';
    emailTemplate.html = body;
    emailTemplate.addFilter('templates', 'enable', 1);
    emailTemplate.addFilter('templates', 'template_id', emailTemplateId);

    sendgrid.send(emailTemplate, (err, json) => {
      const response = (err) ? 'Email Error' : 'Email Success';
      res.send(JSON.stringify({ 'response' : response }));
    });
  }
};
