const stripe = require('stripe')('sk_live_6GKvpYeXbNGAM6RSBQarOZaf');

exports.createPayment = (req, res) => {
  const token = req.headers.authorization;

  if (token !== 'mango') {
    res.status(400);
    res.send(JSON.stringify({ 'isSuccess' : false, 'Message': 'unauthorized' }));
  } else {
    makePayment(req.body, res)
  }
};

makePayment = (req, res) => {
  const { credits, token, email, featured } = req;
  const jobCostPerUnit = 99;
  const featureCostPerUnit = 25;
  const jobCost = parseInt(credits) * jobCostPerUnit;
  const featuredCost = parseInt(featured) * featureCostPerUnit;
  const totalCost = jobCost + featuredCost;

  stripe.charges.create({ 
    amount: `${ totalCost }00`,
    currency: 'gbp',
    description: `Behired payment by customer: ${ email }. Job Cost: £${ jobCost }. Featured Job Cost: £ ${ featuredCost }. Total Cost: £ ${ totalCost }`,
    source: token,
    receipt_email: email,
  }).then((data) => {
    res.status(200).send(JSON.stringify({ 'isSuccess' : true, 'Message' : data }));
  }).catch((error) => {
    res.status(400).send(JSON.stringify({ 'isSuccess' : false, 'Message': error }));
  })
}
