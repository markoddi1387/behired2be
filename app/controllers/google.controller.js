const request = require('request');
const url = 'GoogleApiUrl';

exports.postGoogleJob = (req, res) => {
  const job = req.body;

  request.post(url, { form: job }, (err, response, body) => {
    if (!err && response.statusCode == 200) {
      console.log('Google job - posted successfully');
    } else {
      console.log(err);
    }
  });

  res.send(JSON.stringify({ 'response' : 'Google - Success' }));
};
