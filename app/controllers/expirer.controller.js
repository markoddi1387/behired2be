const database = require("../config/firebase");
const sendgrid = require('sendgrid')('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');
const emailTemplateId = 'dabbcecf-e490-420b-85e8-d3e8acb509c1';

exports.expireJobs = (req, res) => {
  const ref = database.firebaseLeisureStore.collection("behired");

  ref.get().then((jobs) => {
    jobs.forEach((job) => {
      const id = job.id;
      job = job.data().d;

      if (job.datePosted) {
        const datePosted = job.datePosted.toDate();
        const MS_IN_A_DAY = 1000 * 60 * 60 * 24;
        const difference = Date.now() - Date.parse(datePosted);
        let daysDifference = Math.floor(difference/MS_IN_A_DAY);

        if (daysDifference > 60 && job.isActive) {
          // TODO (BUG): Updates job d.isActive field - bug here - this line deletes all and replaces with isActive
        
          // database.firebaseLeisureStore.collection("behired").doc(id).update({ d: { isActive: false } });

          // // Sends company email
          // let emailTemplate = new sendgrid.Email();
          // emailTemplate.addTo(job.email);
          // emailTemplate.subject = 'Job Expired';
          // emailTemplate.from = 'noreply@behired.co.uk';
          // emailTemplate.html = `<p>Your job: ${ job.name }, has now expired.</p><p>Thank you for using Behired.</p>`;
          // emailTemplate.addFilter('templates', 'enable', 1);
          // emailTemplate.addFilter('templates', 'template_id', emailTemplateId);
      
          // sendgrid.send(emailTemplate);
        }
      }
    });

    res.sendStatus(200);
  });
};
