const request = require('request');
const forEach = require('async-foreach').forEach;
const parseString = require('xml2js').parseString;

const database = require("../config/firebase");
const retailApi = 'https://www.cv-library.co.uk/cgi-bin/feed.xml?affid=103532&limit=25&posted=1&q=Trainee%20Fitness%20Instructor&order=date';

exports.getCVLibraryJobs = (req, res) => {
  const options = {
    url: retailApi
  };

  request.get(options, (err, response, body) => {
    if (!err && response.statusCode == 200) {
      let parseString = require('xml2js').parseString;

      parseString(body, (err, result) => {
        const jobs = result.jobs.job;
        addCVLibraryJobs(jobs);
      });
    } else {
      console.log(err);
    }
  });
  res.send('CV Library Downloading...');
};

addCVLibraryJobs = (jobs) => {
  const ref = database.firebaseLeisure.ref('cvlibrary');
  ref.remove();

  forEach(jobs, (job, index, arr) => {
    const tags = `${ job.title.toString().toLowerCase() }, ${ job.company.toString().toLowerCase() }, ${ job.description.toString().toLowerCase() }, retail`;

    const newJob = {
      jobType: 'cvlibrary',
      name: job.title,
      description: job.description,
      address: {
        country: job.country
      },
      url: job.url,
      employmentType: job.jobtype,
      logo: job.image,
      location: job.location,
      resource: job.company,
      validThrough: job.date,
      sector: 'Leisure',
      tags
    };

    ref.push(newJob);
  });
};
