const admin = require("firebase-admin");
const request = require('request');
const forEach = require('async-foreach').forEach;
const database = require("../config/firebase");
const Geohash = require('latlon-geohash');
let counter = 0;

exports.startAdzunaRequests = (req, res) => {
  // res.status(200).send('Adzuna download complete');
  
  counter = 0;

  Promise.all([ 
    getAdzunaJobs('yoga', '1', 'Yoga', 'what'),
    getAdzunaJobs('yoga', '2', 'Yoga', 'what'),
    getAdzunaJobs('yoga', '3', 'Yoga', 'what'),
    getAdzunaJobs('swimming teacher', '1', 'swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '2', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '3', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '4', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '5', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '6', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '7', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '8', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '9', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '10', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '11', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '12', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '13', 'Swimming teacher', 'All'),
    getAdzunaJobs('swimming teacher', '14', 'Swimming teacher', 'All'),
    getAdzunaJobs('lifeguard', '1', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '2', 'Lifeguard', 'All'),
    getAdzunaJobs('personal trainer', '1', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '2', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '3', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '4', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '5', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '6', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '7', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '8', 'Personal trainers', 'All'),
    getAdzunaJobs('personal trainer', '9', 'Personal trainers', 'All'),
    getAdzunaJobs('lifeguard', '3', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '4', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '5', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '6', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '7', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '8', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '9', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '10', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '11', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '12', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '13', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '14', 'Lifeguard', 'All'),
    getAdzunaJobs('lifeguard', '15', 'Lifeguard', 'All'),
    getAdzunaJobs('swim instructor', '1', 'Swimming Teacher', 'All'),
    getAdzunaJobs('swim instructor', '2', 'Swimming Teacher', 'All'),
    getAdzunaJobs('swim instructor', '3', 'Swimming Teacher', 'All'),
    getAdzunaJobs('swim instructor', '4', 'Swimming Teacher', 'All'),
    getAdzunaJobs('swim instructor', '5', 'Swimming Teacher', 'All'),
    getAdzunaJobs('swim instructor', '6', 'Swimming Teacher', 'All'),
    getAdzunaJobs('swim instructor', '7', 'Swimming Teacher', 'All'), 
    getAdzunaJobs('swimming teacher', '1', 'Swimming Teacher', 'Virgin Active'),
    getAdzunaJobs('swimming teacher', '2', 'Swimming Teacher', 'Virgin Active'),
    getAdzunaJobs('swimming teacher', '3', 'Swimming Teacher', 'Virgin Active'),
    getAdzunaJobs('swimming teacher', '4', 'Swimming Teacher', 'Virgin Active'),
    getAdzunaJobs('swimming teacher', '1', 'Swimming Teacher', 'Fitness First'),
    getAdzunaJobs('swimming teacher', '2', 'Swimming Teacher', 'Fitness First'),
    getAdzunaJobs('swimming teacher', '3', 'Swimming Teacher', 'Fitness First'),
    getAdzunaJobs('swimming teacher', '4', 'Swimming Teacher', 'Fitness First'),
    getAdzunaJobs('swimming teacher', '1', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('swimming teacher', '2', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('swimming teacher', '3', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('swimming teacher', '4', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('swimming teacher', '5', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('swimming teacher', '6', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('swimming teacher', '7', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('swimming teacher', '8', 'Swimming Teacher', 'Nuffield Health'),
    getAdzunaJobs('fitness instructor', '1', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('fitness instructor', '2', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('fitness instructor', '3', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('fitness instructor', '4', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('fitness instructor', '5', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '1', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '2', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '3', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '4', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '5', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '6', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '7', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '8', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '9', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '10', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '11', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '12', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '13', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('personal trainer', '14', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('gym trainer', '1', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('gym trainer', '2', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('gym trainer', '3', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('gym trainer', '4', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('gym trainer', '5', 'Personal Trainer', 'The Gym Group'),
    getAdzunaJobs('swimming teacher', '1', 'swimming teacher', 'Fusion Lifestyle'),
    getAdzunaJobs('swimming teacher', '2', 'swimming teacher', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '1', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '2', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '3', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '4', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '5', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '6', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '7', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('lifeguard', '8', 'Lifeguard', 'Fusion Lifestyle'),
    getAdzunaJobs('swimming teacher', '1', 'swimming teacher', 'Serco'),
    getAdzunaJobs('swimming teacher', '2', 'swimming teacher', 'Serco'),
    getAdzunaJobs('lifeguard', '1', 'Lifeguard', 'Serco'),
    getAdzunaJobs('lifeguard', '2', 'Lifeguard', 'Serco'),
    getAdzunaJobs('lifeguard', '3', 'Lifeguard', 'Serco'),
    getAdzunaJobs('lifeguard', '4', 'Lifeguard', 'Serco'),
    getAdzunaJobs('lifeguard', '5', 'Lifeguard', 'Serco'),
    getAdzunaJobs('lifeguard', '6', 'Lifeguard', 'Serco'),
    getAdzunaJobs('lifeguard', '7', 'Lifeguard', 'Serco'),
    getAdzunaJobs('lifeguard', '8', 'Lifeguard', 'Serco'),
    getAdzunaJobs('Everyone Active', '2', 'Swimming Teacher', 'what'),
    getAdzunaJobs('Everyone Active', '1', 'Personal Trainer', 'what'),
    getAdzunaJobs('Everyone Active', '2', 'Personal Trainer', 'what'),
    getAdzunaJobs('Everyone Active', '3', 'Personal Trainer', 'what'),
    getAdzunaJobs('Everyone Active', '4', 'Personal Trainer', 'what'),
    getAdzunaJobs('Everyone Active', '5', 'Personal Trainer', 'what'),
    getAdzunaJobs('Everyone Active', '6', 'Personal Trainer', 'what'),
    getAdzunaJobs('Everyone Active', '1', 'Lifeguard', 'what'),
    getAdzunaJobs('Everyone Active', '2', 'Lifeguard', 'what'),
    getAdzunaJobs('Everyone Active', '3', 'Lifeguard', 'what'),
    getAdzunaJobs('Places for People', '1', 'Personal Trainer', 'what'),
    getAdzunaJobs('Places for People', '1', 'Swimming Teacher', 'what')
  ]).then(() => {
    console.log(counter);
    console.log('complete success');
    res.status(200).send('Adzuna download complete');
  }).catch(() => {
    console.log('Adzuna download error');
    res.status(200).send('Adzuna download complete with errors');
  });
};

getAdzunaJobs = (phrase, page, sector, searchType) => {
  return new Promise((resolve, reject) => { 
    let url = '';

    switch(searchType) {
      case 'All':
        url = `https://api.adzuna.com/v1/api/jobs/gb/search/${ page }?app_id=0f814f67&app_key=bf574e5ebb2455535e2e9178e2808f84&results_per_page=50&what_phrase=${ phrase }&sort_direction=up&sort_by=date`;
        break;
      case 'what':
        url = `https://api.adzuna.com/v1/api/jobs/gb/search/${ page }?app_id=0f814f67&app_key=bf574e5ebb2455535e2e9178e2808f84&results_per_page=50&what=${ phrase }&title_only=${ sector }&sort_direction=up&sort_by=date`;
        searchType = phrase;
        break;
      default:
        url = `https://api.adzuna.com/v1/api/jobs/gb/search/${ page }?app_id=0f814f67&app_key=bf574e5ebb2455535e2e9178e2808f84&results_per_page=50&what_phrase=${ phrase }&company=${ searchType }&sort_direction=up&sort_by=date`;
    }

    request.get(url, (err, response, body) => {
      if (!err && response.statusCode == 200) {
        const jobs = JSON.parse(body);

        console.log('Start Request');
        addAdzunaJobs(jobs.results, phrase, sector, searchType);
        console.log('Done Request');

        resolve();
      } else {
        console.log('Request Error');
        resolve();
      }
    });
  });
};

addAdzunaJobs = (jobs, phrase, sector, searchType) => {  

  forEach(jobs, (job) => {
    let pointHash = null;    
    let geopoint = null;

    const latitude = job.latitude ? job.latitude : null;
    const longitude = job.longitude ? job.longitude : null;
    const titleArray = job.title.toLowerCase().split(' ').map(item => item.trim());
    const sectorArray = sector.toLowerCase().split(' ').map(item => item.trim());
    const phraseArray = phrase.toLowerCase().split(' ').map(item => item.trim());

    const addressTags = `${ job.location.area.join().toLowerCase()}, ${ job.location.display_name.toLowerCase()}`;
    const addressArray = addressTags.split(/[ ,]+/).map(item => item.trim());

    if (latitude !== null && longitude !== null) {
      geopoint = new admin.firestore.GeoPoint(latitude, longitude);
      pointHash = Geohash.encode(latitude, longitude, [9]);
    }

    const pay = (job.salary_is_predicted === '0') ? { currency: '', value: `TBC` } : { currency: '£', value: `${ job.salary_max.toLocaleString() }` }
    const datePosted = (!job.created) ? null : new Date(job.created);
    const brand = searchType;
    const tags = [ ...phraseArray, ...sectorArray, sector.toLowerCase(), 'uk', 'salary', 'Salary', searchType.toLowerCase(), ...titleArray, ...addressArray, phrase.toLowerCase(), brand.toLowerCase() ];

    const newJob = {
      d: {
        datePosted,
        jobType: 'adzuna',
        name: job.title,
        description: job.description,
        url: job.redirect_url,
        sector,
        address: { latitude, longitude, tags: addressTags },
        pay,
        tags,
        brand,
        payType: 'Salary'
      },
      l: geopoint,
      g: pointHash
    };

    counter++;

    const jobBoardRef = database.firebaseLeisureStore.collection('adzuna');
    jobBoardRef.add(newJob);
  });
};
