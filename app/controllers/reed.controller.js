const request = require('request');
const forEach = require('async-foreach').forEach;

const database = require("../config/firebase");
const retailApi = 'https://www.reed.co.uk/api/1.0/search?keywords=swimming%20instructor&resultsToTake=25';
const username = '82af6c37-0e93-4a63-95a7-6c8737f7adff';
const password = '';
const auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');

exports.getReedJobs = (req, res) => {
  const options = {
    url: retailApi,
    headers: {
      'Authorization' : auth
    }
  };

  request.get(options, (err, response, body) => {
    if (!err && response.statusCode == 200) {
      const jobs = JSON.parse(body);
      addReedJobs(jobs.results);
    } else {
      console.log(err);
    }
  });

  res.send('Reed Downloading...');
};

addReedJobs = (jobs) => {
  const ref = database.firebaseLeisure.ref('reed');
  ref.remove();

  forEach(jobs, (job, index, arr) => {
    const tags = `${ job.jobTitle.toLowerCase() }, ${ job.employerName.toLowerCase() }, ${ job.jobDescription.toLowerCase() }, retail`;
    const pay = (job.maximumSalary) ? `£${ job.maximumSalary }` : 'TBC';

    const newJob = {
      jobType: 'reed',
      name: job.jobTitle,
      description: job.jobDescription,
      url: job.jobUrl,
      resource: job.employerName,
      pay,
      sector: 'Leisure',
      tags
    };

    ref.push(newJob);
  });
};
