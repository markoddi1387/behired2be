const builder = require('xmlbuilder');
const database = require("../config/firebase");
const forEach = require('async-foreach').forEach;
const fs = require('fs');

exports.generateXmlFiles = (req, res) => {
  const snapshot = database.firebaseLeisureStore.collection('behired').where('d.isActive', '==', true);
  let listOfJobs = [];

  snapshot.get().then((jobs) => {
    jobs.forEach((job) => {
      let newJob = job.data();
      newJob['id'] = job.id;

      listOfJobs = [ ...listOfJobs, newJob ];
    });

    Promise.all([
      createIndeedXml(listOfJobs),
      createTrovitXml(listOfJobs),
      createAdzunaXml(listOfJobs)
    ]).then(() => {
      res.status(200).send('Adzuna download complete');
    }).catch(() => {
      res.status(200).send('Adzuna download complete with errors');
    });
  });
};

createTrovitXml = (jobs) => {
  return new Promise((resolve, reject) => { 
    let xml = builder.create('trovit', { encoding: 'utf-8' });
    
    forEach(jobs, (job, index) => {
      const id = job.id;
      job = job['d'];

      const url = `https://behired.co.uk/job/${ id }`;
      const address = job.address.addressLine1;

      xml.ele('ad')
      .ele('id').dat(id).up()
      .ele('title').dat(job.name).up()
      .ele('url').dat(url).up()
      .ele('content').dat(job.description).up()
      .ele('city').dat(address).up()
      .ele('salary').dat(job.pay).up()
      .ele('working_hours').dat(8).up()
      .ele('category').dat(job.sector).up()
      .ele('date').dat(job.datePosted.toDate()).up()
      .up()
    });

    const xmlFormatted = xml.end({ pretty: true });
      
    fs.writeFile(`${__dirname}/xml/behired.xml`, xmlFormatted, (err, data) => {
      if (err) {
        console.log(err);
      }
    });

    resolve();
  });
};

createIndeedXml = (jobs) => {
  return new Promise((resolve, reject) => { 
    let xml = builder.create('source', { encoding: 'utf-8' });
    let lastBuildDate = new Date().toLocaleString()
    
    xml.ele('publisher', 'Behired').up()
    xml.ele('publisherurl', 'https://behired.co.uk').up()
    xml.ele('lastBuildDate', lastBuildDate).up()

    forEach(jobs, (job, index) => {
      const id = job.id;
      job = job['d'];

      const url = `https://behired.co.uk/job/${ id }`;
      const address = job.address.addressLine1;

      xml.ele('job')
        .ele('referencenumber').dat(id).up()
        .ele('title').dat(job.name).up()
        .ele('url').dat(url).up()
        .ele('company').dat(job.employerName).up()
        .ele('description').dat(job.description).up()
        .ele('city').dat(address).up()
        .ele('country').dat('England').up()
        .ele('state').dat('UK').up()
        .ele('salary').dat(`£${ job.pay }`).up()
        .ele('email').dat('hello@behired.co.uk').up()
        .ele('jobtype').dat(job.sector).up()
        .ele('date').dat(job.datePosted.toDate()).up()
        .ele('sourcename').dat('behired.co.uk').up()
      .up()
    });

    const xmlFormatted = xml.end({ pretty: true });
      
    fs.writeFile(`${__dirname}/xml/indeed.xml`, xmlFormatted, (err, data) => {
      if (err) {
        console.log(err);
      }
    })

    resolve();
  });
};

createAdzunaXml = (jobs) => {
  return new Promise((resolve, reject) => { 
    let xml = builder.create('jobs', { encoding: 'utf-8' });
    let lastBuildDate = new Date().toLocaleString()
    
    xml.ele('publisher', 'Behired').up()
    xml.ele('publisherurl', 'https://behired.co.uk').up()
    xml.ele('lastBuildDate', lastBuildDate).up()

    forEach(jobs, (job) => {
      const id = job.id;
      job = job['d'];

      const url = `https://behired.co.uk/job/${ id }`;
      const address = job.address.addressLine1;
      const latitude = job.address.latitude;
      const longitude = job.address.longitude;

      xml.ele('job')
        .ele('id').dat(id).up()
        .ele('title').dat(job.name).up()
        .ele('url').dat(url).up()
        .ele('location').dat(address).up()
        .ele('description').dat(job.description).up()
        .ele('country').dat('England').up()
        .ele('company').dat(job.employerName).up()
        .ele('date').dat(job.datePosted.toDate()).up()
        .ele('contract_time').dat('part_time').up()
        .ele('contract_type').dat('contract').up()
        .ele('salary_currency').dat('GBP').up()
        .ele('salary').dat(`£${ job.pay }`).up()
        .ele('salary_frequency').dat('hour').up()
        .ele('geo_lat').dat(latitude).up()
        .ele('geo_lng').dat(longitude).up()
      .up()
    });

    const xmlFormatted = xml.end({ pretty: true });
      
    fs.writeFile(`${__dirname}/xml/adzuna.xml`, xmlFormatted, (err, data) => {
      if (err) {
        console.log(err);
      }
    })
    
    resolve();
  });
};
