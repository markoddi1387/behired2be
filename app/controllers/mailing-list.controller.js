const sendgridToken = 'Bearer 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';
const request = require('request');
const database = require('../config/firebase');

const uri = 'https://api.sendgrid.com/v3/contactdb/recipients';

exports.startMailingList = (req, res) => {
  Promise.all([ 
    this.getDataByEmail('applicants', 'jobseeker'),
    this.getDataByEmail('employers', 'recruiter')
  ]).then((response) => {
    const listOfEmails = [ ...response[0], ...response[1] ];
    this.sendList(listOfEmails);
    res.status(200).send('Mailing complete');
  }).catch(() => {
    console.log('Mailing error');
    res.status(200).send('Mailing complete with errors');
  });
};

exports.getDataByEmail = (type, fieldType) => {
  return new Promise((resolve, reject) => { 
    const ref = database.firebaseLeisure.ref(type);

    ref.on('value', function(snapshot) {
      const jobseekers = Object.values(snapshot.val());

      const listOfEmails = jobseekers.map(jobseeker => {
        return {
          'email': jobseeker.email,
          'first_name': '',
          'last_name': '',
          'type': fieldType
        }
      });

      resolve(listOfEmails);
    }, function (errorObject) {
      console.log('The read failed: ' + errorObject.code);
      resolve([]);
    });
  });
};

exports.sendList = (body) => {
  const headers = { 
    'Authorization': sendgridToken, 
    'Content-Type': 'application/json' 
  };

  request.patch({ headers, uri, body, json: true }, (err, response, body) => {
    if (!err && response.statusCode == 201) {
      console.log('Request Success');
    } else {
      console.log('Request Error');
    }
  });
};

exports.subscribeToBlogMailingList = (req, res) => {
  const { email } = req.body;
  const headers = { 
    'Authorization': sendgridToken, 
    'Content-Type': 'application/json' 
  };
  const body = [{
    email,
    'first_name': '',
    'last_name': '',
    'blog': 'true'    
  }];

  request.post({ headers, uri, body, json: true }, (err, response) => {
    if (!err && response.statusCode == 201) {
      res.status(200).send(JSON.stringify({ 'response' : 'Mailing subscribe success' }));
    } else {
      res.status(400).send(JSON.stringify({ 'response' : 'Mailing subscribe error' }));
    }
  });
};
